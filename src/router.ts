import { FastifyInstance } from 'fastify'

import indexRouter from './controllers/index'

export default async function router(fastify: FastifyInstance) {
  fastify.register(indexRouter, { prefix: '/' }) // http://localhost:3000/
}