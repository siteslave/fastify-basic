import * as fastify from 'fastify'
import * as path from 'path'

const multer = require('fastify-multer')

const envPath = path.join(__dirname, '../config.conf')

require('dotenv').config({ path: envPath })

import routers from './router'

const app: fastify.FastifyInstance = fastify.fastify({
  logger: {
    level: 'info'
  }
})

app.register(multer.contentParser)
app.register(require('fastify-cors'))
app.register(require('fastify-formbody'))

// register knex
app.register(require('./plugins/db'), {
  options: {
    client: 'mysql2',
    connection: {
      host: process.env.DB_HOST || 'localhost',
      port: Number(process.env.DB_PORT) || 3306,
      user: process.env.DB_USER || 'root',
      password: process.env.DB_PASSWORD || '',
      database: process.env.DB_NAME || 'test'
    },
    debug: true
  },
  connectionName: 'db'
})

app.register(require('./plugins/jwt'), {
  secret: process.env.JWT_SECRET || '$#200011124441##@'
})

app.register(require('fastify-static'), {
  root: path.join(__dirname, '../public'),
  prefix: '/assets/'
})

app.register(require('point-of-view'), {
  engine: {
    ejs: require('ejs'),
    root: path.join(__dirname, '../views'),
  },
  includeViewExtension: true
})

app.register(routers)

export default app